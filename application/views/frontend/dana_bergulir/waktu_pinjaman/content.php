<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:cover;">

<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Russo One'; font-size: 40px;color:#ff7700">JANGKA <span style="font-family: 'Russo One';font-size: 35px;color:#000">WAKTU PINJAMAN</span><br></h2>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">

    <div class="card bg-light text-dark">
        <div class="card-body" style="font-family: Roboto">

        <ol type="a">
          <li>Jangka waktu pinjaman dana bergulir maksimal selama 36 (tiga puluh enam) bulan, terhitung sejak tanggak penandatanganan perjanjian pinjaman.</li>
          <li>Perubahan jangka waktu pengembalian pinjaman dituangkan dalam perubahan perjanjian pinjaman.</li>
          <li>Pinjaman dana bergulir oleh penerima pinjaman digunakan untuk modal kerja sebagaimana proposal pengajuan pinjaman.</li>
      </ol>
        </div>
    </div>
<br>
<br>
    
</div>