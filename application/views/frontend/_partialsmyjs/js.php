
    <script type="text/javascript">jssor_1_slider_init();</script>
    <script src="<?php echo base_url()?>assets/js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:800,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Orientation: 2,
                $NoDrag: true
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
 </script>

 <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->

 <!-- #region Jssor Slider Begin -->
    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: https://www.jssor.com -->
    <script src="<?php echo base_url()?>js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:800,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:800,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Orientation: 2,
                $NoDrag: true
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>

 



<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Animate AOS CSS-->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script>
$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
  if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
  }
  var $subMenu = $(this).next(".dropdown-menu");
  $subMenu.toggleClass('show');


  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    $('.dropdown-submenu .show').removeClass("show");
  });


  return false;
});

</script>

<script>
  AOS.init({

    enable: 'mobile'
  });
</script>

<!--Scroll Top -->
<script type="text/javascript">
	$(document).ready(function(){
	$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-to-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 400);
			return false;
		});
});
</script>


<!--Scroll Spy-->
<script>
$(document).ready(function(){
  // Add scrollspy to <body>
  $('body').scrollspy({target: ".navbar", offset: 50});   

  // Add smooth scrolling on all links inside the navbar
  $("#myNavbar a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    }  // End if
  });
});
</script>
<!--End Scroll Spy-->


<script type="text/javascript">
  $(document).ready(function() {
    $('#pmdg').DataTable({
        columnDefs: [
    {targets: [ 0 ], visible: false }
    
    ] 

    });
} );
  
   $(document).ready(function() {
     $('#uud').DataTable( {
         columnDefs: [
    {targets: [ 0 ], visible: false }
    
    ] 
    } );

} );

    $(document).ready(function() {
    $('#ppt').DataTable(
      {
           columnDefs: [
    {targets: [ 0 ], visible: false }
    
    ],

     "aaSorting": [[ 0, "asc" ],[ 1, 'asc' ]]
      });
});

     $(document).ready(function() {
    $('#kpwk').DataTable({
      columnDefs: [
    {targets: [ 0 ], visible: false }
    
    ] 
    });
} );

      $(document).ready(function() {
    $('#pp').DataTable({
       columnDefs: [
    {targets: [ 0 ], visible: false }
    
    ] 
    });
} );

       $(document).ready(function() {
    $('#pmk').DataTable({
      columnDefs: [
    {targets: [ 0 ], visible: false }
    
    ] 
    });
} );

        $(document).ready(function() {
    $('#perda').DataTable({
     
      columnDefs: [
    {targets: [ 0 ], visible: false }
    
    ],

     "aaSorting": [[ 0, "asc" ],[ 1, 'asc' ]]
    });
} );

         $(document).ready(function() {
    $('#perwako').DataTable({
      columnDefs: [
      {targets: [ 0 ], visible: false }
      ],
     "aaSorting": [[ 0, "asc" ],[ 1, 'asc' ]]
      
    });
} );

          $(document).ready(function() {
    $('#semdn').DataTable();
} );

           $(document).ready(function() {
    $('#semk').DataTable({
      columnDefs: [
      {targets: [ 0 ], visible: false }
      ],
     "aaSorting": [[ 0, "asc" ],[ 1, 'asc' ]]
    });
} );

            $(document).ready(function() {
    $('#sewako').DataTable({
      columnDefs: [
      {targets: [ 0 ], visible: false }
      ],
     "aaSorting": [[ 0, "asc" ],[ 1, 'asc' ]]
    });
} );

            $(document).ready(function() {
    $('#lkaudit').DataTable();
} );

            $(document).ready(function() {
    $('#lkunaudit').DataTable();
} );

             $(document).ready(function() {
    $('#opinibpk').DataTable();
} );
             $(document).ready(function() {
    $('#blud').DataTable();
} );

              $(document).ready(function() {
    $('#renstra').DataTable({
      columnDefs: [
      {targets: [ 0 ], visible: true }
      ],
     "aaSorting": [[ 0, "asc" ],[ 1, 'asc' ]]

    });
} );

$(document).ready(function() {
    $('#apbd').DataTable({
         initComplete: function () {

            this.api().columns(0).every( function () {
                var column = this;
                var select = $('<select><option value="">Semua Data PerTahun</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }


    });
} );
    

    $(document).ready(function() {
    $('#anggaran').DataTable({
         initComplete: function () {

            this.api().columns(0).every( function () {
                var column = this;
                var select = $('<select><option value="">Semua Data PerTahun</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }


    });
} );
    
           
</script>
<script type="text/javascript" src="<?php echo base_url('assets/js/youmax.min.js') ?>"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script> -->
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

<body>
  
</body>
</html>