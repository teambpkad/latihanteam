<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:cover;">
<br>
<br>
<br>

<br>


<!--VISI-->
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">VISI</h2>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
<p style="text-align: justify; text-justify: inter-word">
	Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam Tahun 2016-2021 adalah :
	<br>
	<div class="card bg-light text-dark">
    <div class="card-body">
<?php
$query = $this->db->query("SELECT * FROM visi_bpkad");

$row = $query->row();

if (isset($row))
{       
        echo $row->isi_visi;
}
?>
</div></div>
</p>
</div>
<br>
<br>

<!--MISI-->
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-offset="-50"
    data-aos-delay="100"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">MISI</h2>
</center>

<div class="container" data-aos="fade-in"
data-aos-offset="-50"
    data-aos-delay="100"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
<p style="text-align: justify; text-justify: inter-word">
	Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam adalah sebagai berikut :
	<div class="card bg-light text-dark">
    <div class="card-body">
<ol>
<?php
$query = $this->db->get('misi_bpkad');

foreach ($query->result() as $row)
{
        echo $row->isi_misi;
}
?>
</ol>
</div>
</div>
</p>

</div>

<br>
<br>

<!--STRATEGI-->
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-offset="-50"
    data-aos-delay="100"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">STRATEGI</h2>
</center>

<div class="container" data-aos="fade-in"
data-aos-offset="-50"
    data-aos-delay="100"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
<p style="text-align: justify; text-justify: inter-word">
	<div class="card bg-light text-dark">
    <div class="card-body">
        <ol>
<?php
$query = $this->db->get('strategi_bpkad');

foreach ($query->result() as $row)
{
        echo $row->isi_strategi; 
}
?>
</ol>
</div>
</div>
</p>
</div>

<br>
<br>

<!--KEBIJAKAN-->
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-offset="-50"
    data-aos-delay="100"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">KEBIJAKAN</h2>
</center>

<div class="container" data-aos="fade-in"
data-aos-offset="-50"
    data-aos-delay="100"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
<p style="text-align: justify; text-justify: inter-word">
	<div class="card bg-light text-dark">
    <div class="card-body">
        <ol>
        <?php
        $query = $this->db->get('kebijakan_bpkad');

        foreach ($query->result() as $row)
        {               
                echo $row->isi_kebijakan;                
        }   
        ?>
    </ol>
</div>
</div>
</p>
</div>
<br>
<br>
<br>
</div>