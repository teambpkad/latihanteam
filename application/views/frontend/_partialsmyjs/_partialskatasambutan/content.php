<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size: cover;">
<br>
<br>
<br>

<br>

<!--KEBIJAKAN-->
<center>
    <h2 class="container" data-aos="zoom-in-down"
    data-aos-offset="-50"
    data-aos-delay="100"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Russo One', cursive">KATA SAMBUTAN</h2>
</center>

<div class="container" data-aos="fade-up"
data-aos-offset="-50"
    data-aos-delay="100"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
<p style="text-align: justify; text-justify: inter-word">
	<div class="card bg-light text-dark">
    <div class="card-body text-justify" style="font-family: 'Roboto Condensed'">


Assalamu'alaikum Wr. Wb<br>
Alhamdulillahi Rabbil Aalamin<br>
Washshalaatu Wassalaamu 'alaa Asyrafil Albiyaa'i Wal Murasliin<br>
Wa'alaa Aalihi wa shahbihi Ajma'iin.<br><br>

Alhamdulillah, Puji Syukur kepada Allah SWT, yang telah memberikan karunia-Nya kepada kita semua berupa nikmat iman dan Islam.<br><br>

Shalawat beriring salam tak bosan-bosannya kita sampaikan kepangkuan baginda Rasulullah Nabi Besar Muhammad SAW, Para Sahabat dan keluarga beliau serta pengikutnya yang istiqomah. <br><br>

<b>Selamat Datang di Website Badan Pengelola Keuangan dan Aset Daerah Kota Batam.</b><br><br>

Website ini dimaksudkan sebagai sarana publikasi untuk memberikan informasi dan gambaran Badan Pengelola Keuangan dan Aset Daerah (BPKAD) Kota Batam dalam melaksanakan Pengelolaan Keuangan dan Aset Daerah. Melalui keberadaan website ini kiranya masyarakat dapat mengetahui seluruh informasi tentang Kebijakan Pemerintah Kota Batam didalam Pengelolaan sektor Anggaran Keuangan & Aset Daerah di wilayah Pemerintah Kota Batam.<br><br>

Kritik dan saran terhadap kekurangan dan kesalahan yang ada sangat kami harapkan, guna penyempurnaan Website ini dimasa yang akan datang. Semoga Website ini dapat memberikan manfaat bagi kita semua. <br><br>

Terima kasih


</div>
</div>
</p>
</div>


<br>
<br>
<br>
</div>