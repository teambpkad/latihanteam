<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size: cover;">
<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Russo One', cursive">KRITERIA DAN PERSYARATAN <span style="font-size: 40px;color:#ff7700">KOPERASI</span></h2><br>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">

    <div class="card bg-light text-dark" style="font-family: 'Roboto'">
        <div class="card-body">
            <h3>Kriteria Koperasi :</h3>
            <ol type="a">

            <li>Mempunyai akta pendirian/perubahan anggaran dasar sesuai dengan ketentuan yang berlaku; </li>
            <li>Telah memiliki badan hukum yang sudah disahkan oleh Pemerintah; </li>
            <li> Telah melaksanakan rapat anggota tahunan (RAT) tahun buku terakhir dilengkapi dengan Neraca dan perhitungan hasil usaha koperasi dan susunan pengurus/pengawas;</li>
            <li>Untuk unit usaha simpan pinjam wajib memiliki modal sendiri minimal <b>Rp. 15.000.000,- (lima belas juta rupiah)</b> dan dikelola secara terpisah yang dibuktikan dalam neraca tahun buku berjalan; dan</li>
            <li>Diutamakan bagi koperasi yang memiliki peringkat minimal cukup sehat;</li>
        </ol>
            <br>
            <h3>Persyaratan Koperasi :</h3>
            <ol type="a">
            <li>Pemohon mengajukan proposal dan mengisi formulir;</li>
            <li>Melampirkan foto copy Akta Pendirian/akta Perubahan anggaran dasar koperasi;</li>
            <li>Melampirkan foto copy SK pengesahan badan hukum/SK perubahan anggaran dasar koperasi yang dibuat sesuai dengan ketentuan yang berlaku;</li>
            <li>Melampirkan laporan rapat anggota tahunan (RAT) tahun buku terakhir;</li>
            <li>Melampirkan laporan keuangan (Neraca dan Rugi Laba);</li>
            <li>Melampirkan foto copy tanda daftar umum koperasi (TDUK) dan nomor pokok wajib pajak (NPWP);</li>
            <li>Selain koperasi simpan pinjam wajib melampirkan foto copy surat izin usaha perdagangan (SIUP);</li>
            <li>Melampirkan foto copy rekening Bank yang ditunjuk;</li>
            <li>Melampirkan foto copy KTP Pengurus yang masih berlaku;</li>
            <li>Melampirkan foto copy Surat Keterangan domisili usaha dari Instansi yang berwenang;</li>
            <li>Pas foto terbaru Pengurus masing-masing ukuran 3x4 cm sebanyak 2 lembar;</li>
            <li>Melampirkan rencana penyaluran atau penggunaan dana pinjaman;</li>
            <li>Melampirkan pernyataan keputusan rapat (PKR) yang dibuatkan oleh notaris pembuatan akta koperasi yang menyatakan bahwa   rapat anggota menyetujui untuk pengajuan pinjam dana bergulir; dan</li>
            <li>Melampirkan foto copy dokumen jaminan;</li>
    </div>
</div>
<br>
<br>
    
</div>
</div>
