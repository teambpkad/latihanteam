     <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
          try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

        <ul class="nav nav-list">
          <li class="">
            <a href="<?php echo base_url('admin/dashboard_admin')?>">
              <i class="menu-icon fa fa-tachometer"></i>
              <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('admin/data_user')?>" class="dropdown-toggle">
              <i class="menu-icon fa fa-desktop"></i>
              <span class="menu-text">
                Administrator
              </span>

              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
               <li class="">
                <a href="<?php echo base_url('admin/data_user')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  User
                </a>

                <b class="arrow"></b>
              </li>
               <li class="">
                <a href="#" class="dropdown-toggle">
                  <i class="menu-icon fa fa-caret-right"></i>

                  Profile
                  <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                  <li class="">
                    <a href="<?php echo base_url('admin/data_visi')?>">
                      <i class="menu-icon fa fa-caret-right"></i>
                      Visi
                    </a>

                    <b class="arrow"></b>
                  </li>

                  <li class="">
                    <a href="<?php echo base_url('admin/data_misi')?>">
                      <i class="menu-icon fa fa-caret-right"></i>
                      Misi
                    </a>

                    <b class="arrow"></b>
                  </li>

                   <li class="">
                    <a href="<?php echo base_url('admin/data_strategi')?>">
                      <i class="menu-icon fa fa-caret-right"></i>
                      Strategi
                    </a>

                    <b class="arrow"></b>
                  </li>

                    <li class="">
                    <a href="<?php echo base_url('admin/data_kebijakan')?>">
                      <i class="menu-icon fa fa-caret-right"></i>
                      Kebijakan
                    </a>

                    <b class="arrow"></b>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          
          <li class="active">
            <a href="<?php echo base_url('admin/data_profile_jabatan')?>">
              <i class="menu-icon fa fa-pencil-square-o"></i>
              <span class="menu-text">Profil Pejabat</span>
            </a>

            <b class="arrow"></b>
          </li>
          
           <li class="">
            <a href="<?php echo base_url('admin/data_regulasi_keuda')?>">
              <i class="menu-icon fa fa-list"></i>
              <span class="menu-text">Regulasi Keuda</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('admin/data_foto_kegiatan')?>">
              <i class="menu-icon fa fa-picture-o"></i>
              <span class="menu-text"> Galeri Kegiatan </span>
            </a>

            <b class="arrow"></b>
          </li> 

          <li class="">
            <a href="<?php echo base_url('admin/data_renstra')?>" class="dropdown-toggle">
              <i class="menu-icon fa fa-desktop"></i>
              <span class="menu-text">
                Publikasi
              </span>

              <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
               <li class="">
                <a href="<?php echo base_url('admin/data_renstra')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Data Renstra
                </a>

                <b class="arrow"></b>
              </li>
               
            </ul>

            <ul class="submenu">
               <li class="">
                <a href="<?php echo base_url('admin/data_lkpj')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  LKPJ
                </a>

                <b class="arrow"></b>
              </li>
               
            </ul>

            <ul class="submenu">
               <li class="">
                <a href="<?php echo base_url('admin/data_lkpd')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  LKPD
                </a>

                <b class="arrow"></b>
              </li>
               
            </ul>

            <ul class="submenu">
               <li class="">
                <a href="<?php echo base_url('admin/data_apbd')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  APBD
                </a>

                <b class="arrow"></b>
              </li>
               
            </ul>

            <ul class="submenu">
               <li class="">
                <a href="<?php echo base_url('admin/data_hibah')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Hibah
                </a>

                <b class="arrow"></b>
              </li>
               
            </ul>

             <ul class="submenu">
               <li class="">
                <a href="<?php echo base_url('admin/data_anggaran')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Anggaran
                </a>

                <b class="arrow"></b>
              </li>
               
            </ul>

            <ul class="submenu">
               <li class="">
                <a href="<?php echo base_url('admin/data_blud')?>">
                  <i class="menu-icon fa fa-caret-right"></i>
                  Dana Bergulir
                </a>

                <b class="arrow"></b>
              </li>
               
            </ul>
          </li>
        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
          <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
      </div>