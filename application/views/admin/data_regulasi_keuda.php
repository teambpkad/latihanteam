<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Regulasi Keuda</li>
      </ul><!-- /.breadcrumb -->

<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Regulasi Keuda administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="pull-left tableTools-container">
        <div class="dt-buttons btn-overlap btn-group">
            <button class="btn btn-lg btn-success" data-toggle="modal" data-target="#tambahkeuda">
                      <i class="ace-icon fa fa-plus"></i>
                      Regulasi Keuda
            </button>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                      <div>
                        <table id="regulasi-table" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                              <th style="text-align:center">No</th>
                              <th style="text-align:center">Kategori Keuda</th>
                              <th style="text-align:center">Judul Keuda</th>
                              <th style="text-align:center">Uraian Keuda</th>
                              <th style="text-align:center">Tahun</th>
                              <th style="text-align:center">Aksi</th>
                            </tr>
                          </thead>

                          <tbody>
                            <?php foreach ($req_keuda as $key => $kd):

                              $no=1;
                            ?>
                             </tr>
                                <td><?php  echo $no+$key ?></td>
                                <td><?php  echo $kd->ktg_keuda ?></td>
                                <td><?php  echo $kd->nm_keuda ?></td>
                                <td><?php  echo $kd->uraian_keuda ?></td>
                                <td><?php  echo $kd->tahun ?></td>
                                <td>
                                  <div class="hidden-sm hidden-xs action-buttons">
                                    <a class="green" href="<?php echo base_url('admin/data_regulasi_keuda/edit/'.$kd->id_keuda)?>">
                                      <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                    <a class="red" href="<?php echo base_url('admin/data_regulasi_keuda/hapus/'.$kd->id_keuda)?>">
                                      <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                    </a>

                                  </div>
                                  <div class="hidden-md hidden-lg">
                                    <div class="inline pos-rel">
                                      <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                        <i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
                                      </button>
                                      <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                        <li>
                                          <a href="<?php echo base_url('admin/data_regulasi_keuda/edit/'.$kd->id_keuda)?>" class="tooltip-success" data-rel="tooltip" title="Edit">
                                            <span class="green">
                                              <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="<?php echo base_url('admin/data_regulasi_keuda/hapus/'.$kd->id_keuda)?>" class="tooltip-error" data-rel="tooltip" title="Delete">
                                            <span class="red">
                                              <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </td>
                          </tr>
  <!--ace-icon fa fa-pencil-->
                              <?php endforeach ?>
                          </tbody>
                </table>
                       
                    </div>

                  </div>
                </div>
              </div>                  
            </div>  
          </div>
        </div>
      </div>  

      <div class="modal fade" id="tambahkeuda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <center><h5 class="modal-title" id="exampleModalLabel">FORM INPUT KEUDA</h5></center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <form action="<?php echo base_url(). 'admin/data_regulasi_keuda/tambah_aksi'?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label>Kategori Keuda</label>
                <select type="text" name="ktg_keuda" class="form-control">
                  <option>Undang Undang</option>
                  <option>Peraturan Pemerintah</option>
                  <option>Peraturan Presiden</option>
                  <option>Peraturan Menteri Keuangan</option>
                  <option>Peraturan Menteri Dalam Negeri</option>
                  <option>Keputusan Menteri Dalam Negeri</option>
                  <option>Keputusan Menteri Keuangan</option>
                  <option>Peraturan Daerah</option>
                  <option>Peraturan Walikota</option>
                  <option>Keputusan Walikota</option>
                  <option>Surat Edaran Menteri Dalam Negeri</option>
                  <option>Surat Edaran Menteri Keuangan</option>
                  <option>Surat Edaran Walikota</option>
                </select>
              </div>

              <div class="form-group">
                <label>Judul Keuda</label>
                <input type="text" name="nm_keuda" class="form-control" required="">
              </div>

              <div class="form-group">
                <label>Uraian Keuda</label>
                <Textarea type="text" name="uraian_keuda" class="form-control"></Textarea>
              </div>

              <div class="form-group">
                <label>Tahun</label>
                <input type="text" name="tahun" class="form-control" required="">
              </div>

              <div class="form-group">
                <label>File</label>
                <input type="file" name="file" class="form-control" required="">
              </div>
          </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>

            </form>
        </div>
      </div>
    </div>           
</div>
</div>