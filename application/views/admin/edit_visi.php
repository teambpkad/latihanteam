<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Edit Data Visi</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Visi administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

	<div class="content-wrapper">
		<div class="container-fluid">
			<center><h3></i>EDIT DATA VISI</h3></center>

			<?php foreach ($visi as $key => $vs) : ?>
				<form method="post"  action="<?php echo base_url(). 'admin/data_visi/update' ?>">
					<div class="form-group">
						<label>VISI</label>
						<input type="hidden" name="id_visi" class="form-control" value="<?php echo $vs->id_visi?>">
						<input type="text" name="isi_visi" class="form-control" value="<?php echo $vs->isi_visi?>">
					</div>
					<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
				</form>

			<?php endforeach;?>
		</div>
	</div>
</div>
</div>