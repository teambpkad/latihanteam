<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Kebijakan</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Kebijakan administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

	<div class="content-wrapper">
		<div class="container-fluid">
			<center><h3></i>EDIT DATA KEBIJAKAN</h3></center>

			<?php foreach ($kebijakan as $key => $kbk) : ?>
				<form method="post"  action="<?php echo base_url(). 'admin/data_kebijakan/update' ?>">
					<div class="form-group">
						<label>Kebijakan</label>
						<input type="hidden" name="id_kebijakan" class="form-control" value="<?php echo $kbk->id_kebijakan?>">
						<input type="text" name="isi_kebijakan" class="form-control" value="<?php echo $kbk->isi_kebijakan?>">
					</div>
					<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
				</form>

			<?php endforeach;?>
		</div>
	</div>	
</div>
</div>