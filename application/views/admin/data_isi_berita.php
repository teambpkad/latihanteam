<div class="content-wrapper">
  <div class="container-fluid">
      <button class="btn btn-sm btn-primary mb-2 mt-2" data-toggle="modal" data-target="#tambahberita"><i class="fas fa-plus fa-sm"></i> Tambah Berita</button>
        <div class="row">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="text-align:center">no</th>
                <th style="text-align:center">Kategori Berita</th>
                <th style="text-align:center">Username</th>
                <th style="text-align:center">Nama Berita</th>
                <th style="text-align:center">Sumber Berita</th>
                <th style="text-align:center">Tanggal Posting</th>
                <th style="text-align:center">Isi Berita</th>
                <th style="text-align:center">Foto Berita</th>
                <th style="text-align:center">Status Berita</th>
                <th colspan="3" style="text-align:center">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($isi_berita as $key => $isi_brt):

                  $no=1;
                ?>
               </tr>
                  <td><?php  echo $no+$key ?></td>
                  <td><?php  echo $isi_brt->nm_ktg ?></td>
                  <td><?php  echo $isi_brt->username ?></td>
                  <td><?php  echo $isi_brt->nm_brt ?></td>
                  <td><?php  echo $isi_brt->smr_brt ?></td>
                  <td><?php  echo $isi_brt->tgl_pst ?></td>
                  <td><?php  echo $isi_brt->isi_brt ?></td>
                  <td><?php  echo $isi_brt->foto_brt  ?></td>
                  <td><?php  echo $isi_brt->status_brt  ?></td>
                  <td style="text-align:center"><?php echo anchor('admin/data_isi_berita/edit/'. $isi_brt->id_brt, '<div class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></div>') ?></td>
                  <td style="text-align:center"><?php echo anchor('admin/data_isi_berita/hapus/'. $isi_brt->id_brt, '<div class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></div>')?></td>
               </tr>

              <?php endforeach ?>
            </tbody>
          </table>
        </div>
  </div>  
</div>


  <!-- Modal -->
<div class="modal fade" id="tambahberita" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <center><h5 class="modal-title" id="exampleModalLabel">FORM INPUT BERITA</h5></center>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(). 'admin/data_user/tambah_aksi'?>" method="post" enctype="multipart/form-data">

          <div class="form-group">
            <label>Kategori Berita</label>
              <select name="nm_ktg" id="id_ktg_brt" class="form-control">
                  <?php
                     $categories = $this->model_isi_berita->getAllCategories();
                     foreach($categories as $category)
                     {
                   ?>
                      <option value="<?php echo $category->id_ktg_brt; ?>"><?php echo $category->nm_ktg; ?></option>    
                  <?php
                    }
                  ?>  
              </select>
          </div>

          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" id="id_user" class="form-control">
                  <?php
                     $user = $this->model_isi_berita->getUser();
                     foreach($user as $user)
                     {
                   ?>
                      <option value="<?php echo $user->id_user; ?>"><?php echo $user->username; ?></option>    
                  <?php
                    }
                  ?>
          </div>

          <div class="form-group">
            <label>Judul Berita</label>
            <input type="text" name="nm_brt" class="form-control">
          </div>

          <div class="form-group">
            <label>Sumber Berita</label>
            <input type="text" name="smr_brt" class="form-control">
          </div>

          <div class="form-group">
            <label>Tanggal Berita</label>
            <input type="date" name="tgl_pst" class="form-control">
          </div>

          <div class="form-group">
            <label>Isi Berita</label>
            <input type="text" name="isi_brt" class="form-control">
          </div>
      </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
    </div>
  </div>
</div>

</div>