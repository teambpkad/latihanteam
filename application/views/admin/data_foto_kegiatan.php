<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Foto Kegiatan</li>
      </ul><!-- /.breadcrumb -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Foto Kegiatan administrator bpkad-batam
          </small>
        </h1>
      </div>
      <!-- /.page-header -->
      <div class="row">
        <div class="col-xs-12">
          <div class="pull-left tableTools-container">
            <div class="dt-buttons btn-overlap btn-group">
                <button class="btn btn-lg btn-success" data-toggle="modal" data-target="#tambahfoto">
                          <i class="ace-icon fa fa-plus"></i>
                          Foto Kegiatan
                </button>
            </div>
          </div>

          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                    <!-- div.table-responsive -->
                    <!-- div.dataTables_borderWrap -->
                      <div>
                        <table id="foto-table" class="table table-striped table-bordered" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th style="text-align:center">No</th>
                              <th style="text-align:center">Judul Kegiatan</th>
                              <th style="text-align:center">Keterangan</th>
                              <th style="text-align:center">Foto</th>
                              <th style="text-align:center">Tanggal Kegiatan</th>
                              <th style="text-align:center">Status</th>
                              <th style="text-align:center">Aksi</th>
                            </tr>
                          </thead>

                          <tbody>
                        
                            <?php foreach ($ft_kegiatan as $key => $ft):

                              $no=1;
                            ?>
                             </tr>
                                <td><?php  echo $no+$key ?></td>
                                <td><?php  echo $ft->jdl_kegiatan ?></td>
                                <td><?php  echo $ft->keterangan ?></td>
                                <td><img style="width:150px;height: 100px " src="<?php echo base_url().'images/'.$ft->foto;?>"/></td>
                                <td><?php  echo $ft->tgl_kegiatan ?></td>
                                <td><?php  echo $ft->status ?></td>
                               <td>
                                  <div class="hidden-sm hidden-xs action-buttons">
                                    <a class="green" href="<?php echo base_url('admin/data_foto_kegiatan/edit/'.$ft->id_ft_kegiatan)?>">
                                      <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                    <a class="red" href="<?php echo base_url('admin/data_foto_kegiatan/hapus/'.$ft->id_ft_kegiatan)?>">
                                      <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                    </a>
                                  </div>

                                  <div class="hidden-md hidden-lg">
                                    <div class="inline pos-rel">
                                      <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                        <i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
                                      </button>
                                      <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                        <li>
                                          <a href="<?php echo base_url('admin/data_foto_kegiatan/edit/'.$ft->id_ft_kegiatan)?>" class="tooltip-success" data-rel="tooltip" title="Edit">
                                            <span class="green">
                                              <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="<?php echo base_url('admin/data_foto_kegiatan/hapus/'.$ft->id_ft_kegiatan)?>" class="tooltip-error" data-rel="tooltip" title="Delete">
                                            <span class="red">
                                              <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                            </td>
                          </tr>
  <!--ace-icon fa fa-pencil-->
                              <?php endforeach ?>
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
               </div>                  
              </div>  
            </div>         
            <!-- Modal -->
            <div class="modal fade" id="tambahfoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <center><h5 class="modal-title" id="exampleModalLabel">FORM INPUT FOTO KEGIATAN</h5></center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="<?php echo base_url(). 'admin/data_foto_kegiatan/tambah_aksi'?>" method="post" enctype="multipart/form-data">

              <div class="form-group">
                <label>Judul Kegiatan</label>
                <input type="text" name="jdl_kegiatan" class="form-control" required="">
              </div>

              <div class="form-group">
                <label>Keterangan</label>
                <input type="text" name="keterangan" class="form-control" required="">
              </div>

              <div class="form-group">
            <label>Foto Kegiatan</label>
            <input type="file" name="foto" class="form-control" required="">
          </div>

          <div class="form-group">
            <label>Foto Kegiatan</label>
            <input type="file" name="foto2" class="form-control" required="">
          </div>

          <div class="form-group">
            <label>Foto Kegiatan</label>
            <input type="file" name="foto3" class="form-control" required="">
          </div>

              <div class="form-group">
                <label>Tanggal Kegiatan</label>
                <input type="date" name="tgl_kegiatan" class="form-control" required="">
              </div>

              <div class="form-group">
                <label>Status Terbit</label>
                <select type="text" name="status" class="form-control">
                  <option>No</option>
                  <option>Yes</option>
                </select>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
        </div>
      </div>
    </div>
<!--page <table></table>-->
<!-- isi content admin/data_user -->
          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->


