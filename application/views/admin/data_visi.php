<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data user</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data user administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

    <div class="content-wrapper">
      <div class="container-fluid">
              <div class="row">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="text-align:center">No</th>
                    <th style="text-align:center">Isi Visi</th>
                    <th colspan="3" style="text-align:center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                 <?php foreach ($visi as $key => $vs):

                      $no=1;
                    ?>
                   </tr>
                      <td><?php  echo $no+$key ?></td>
                      <td><?php  echo $vs->isi_visi ?></td>
                      <td> 
                        <div class="hidden-sm hidden-xs action-buttons">
                                    <a class="green" href="<?php echo base_url('admin/data_visi/edit/'.$vs->id_visi)?>">
                                      <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                    <a class="red" href="<?php echo base_url('admin/data_visi/hapus/'.$vs->id_visi)?>">
                                      <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                    </a>
                                  </div>

                                  <div class="hidden-md hidden-lg">
                                    <div class="inline pos-rel">
                                      <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                        <i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
                                      </button>
                                      <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                        <li>
                                          <a href="<?php echo base_url('admin/data_visi/edit/'.$vs->id_visi)?>" class="tooltip-success" data-rel="tooltip" title="Edit">
                                            <span class="green">
                                              <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="<?php echo base_url('admin/data_visi/hapus/'.$vs->id_visi)?>" class="tooltip-error" data-rel="tooltip" title="Delete">
                                            <span class="red">
                                              <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                      </td>
                   </tr>

                  <?php endforeach ?>
                 
                </tbody>
              </table>
            </div>
      </div>  
    </div>
  </div>
</div>