<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Edit Data Misi</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Misi administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->
	

	<div class="content-wrapper">
		<div class="container-fluid">
			<center><h3></i>EDIT DATA MISI</h3></center>

			<?php foreach ($misi as $key => $ms) :?>

				<form method="post"  action="<?php echo base_url(). 'admin/data_misi/update' ?>">
					<div class="form-group">
						<label>MISI</label>
						<input type="hidden" name="id_misi" class="form-control" value="<?php echo $ms->id_misi?>">
						<input type="text" name="isi_misi" class="form-control" value="<?php echo $ms->isi_misi?>">
						
					</div>
					<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
				</form>

			<?php endforeach;?>
		</div>
	</div>	
</div>
</div>