
<div class="content-wrapper">
  <div class="container-fluid">
      <button class="btn btn-sm btn-primary mb-2 mt-2" data-toggle="modal" data-target="#tambahktg"><i class="fas fa-plus fa-sm"></i> Tambah Kategori Berita</button>
        <div class="row">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="text-align:center">no</th>
                <th style="text-align:center">Nama Kategori</th>
                <th style="text-align:center">Keterangan</th>
                <th colspan="3" style="text-align:center">Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($kategori_berita as $key => $ktg):

                  $no=1;
                ?>
               </tr>
                  <td><?php  echo $no+$key ?></td>
                  <td><?php  echo $ktg->nm_ktg ?></td>
                  <td><?php  echo $ktg->keterangan ?></td>
                  <td style="text-align:center"><?php echo anchor('admin/data_kategori_berita/edit/'. $ktg->id_ktg_brt, '<div class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></div>') ?></td>
                  <td style="text-align:center"><?php echo anchor('admin/data_kategori_berita/hapus/'. $ktg->id_ktg_brt, '<div class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></div>')?></td>
               </tr>

              <?php endforeach ?>
            </tbody>
          </table>
        </div>
  </div>  
</div>

 <!-- Modal -->
<div class="modal fade" id="tambahktg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <center><h5 class="modal-title" id="exampleModalLabel">FORM INPUT KATEGORI BERITA</h5></center>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(). 'admin/data_kategori_berita/tambah_aksi'?>" method="post" enctype="multipart/form-data">

          <div class="form-group">
            <label>Nama Ketegori</label>
            <input type="text" name="nm_ktg" class="form-control">
          </div>

          <div class="form-group">
            <label>Keterangan</label>
            <input type="text" name="keterangan" class="form-control">
          </div>

      </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
    </div>
  </div>
</div>
  
</div>