<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Edit Data Foto Kegiatan</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Foto Kegiatan administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->
	

	<div class="content-wrapper">
		<div class="container-fluid">
			<center><h3></i>Edit Data Foto Kegiatan</h3></center>

			<?php foreach ($ft_kegiatan as $key => $ft):?>

				<form method="post"  action="<?php echo base_url(). 'admin/data_foto_kegiatan/update' ?>">
					<div class="form-group">
						<label>Judul Kegiatan</label>
						<input type="hidden" name="id_ft_kegiatan" class="form-control" value="<?php echo $ft->id_ft_kegiatan?>">
						<input type="text" name="jdl_kegiatan" class="form-control" value="<?php echo $ft->jdl_kegiatan?>">
					</div>

          <div class="form-group">
              <label>Keterangan</label>
              <input type="text" name="keterangan" class="form-control" value="<?php echo $ft->keterangan?>">
          </div>

          <div class="form-group">
                <label>Status Terbit</label>
                <select type="text" name="status" class="form-control">
                  <option>No</option>
                  <option>Yes</option>
                </select>
          </div>
					<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
				</form>

			<?php endforeach;?>
		</div>
	</div>	
</div>
</div>