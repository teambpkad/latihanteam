<div class="content-wrapper">
	<div class="container-fluid">
		<center><h3><i class="fas fa-edit"></i>EDIT DATA KATEGORI BERITA</h3></center>

		<?php foreach ($kategori_berita as $key => $ktg) : ?>
			<form method="post"  action="<?php echo base_url(). 'admin/data_kategori_berita/update' ?>">
				<div class="form-group">
					<label>Nama Kategori</label>
					<input type="hidden" name="id_ktg_brt" class="form-control" value="<?php echo $ktg->id_ktg_brt?>">
					<input type="text" name="nm_ktg" class="form-control" value="<?php echo $ktg->nm_ktg?>">
				</div>

				<div class="form-group">
					<label>Keterangan</label>
					<input type="hidden" name="id_ktg_brt" class="form-control" value="<?php echo $ktg->id_ktg_brt?>">
					<input type="text" name="keterangan" class="form-control" value="<?php echo $ktg->keterangan?>">
				</div>
				<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
			</form>

		<?php endforeach;?>
	</div>
</div>	