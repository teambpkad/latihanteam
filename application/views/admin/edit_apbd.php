<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data APBD</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Apbd administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

		<div class="content-wrapper">
			<div class="container-fluid">
				<center><h3></i>EDIT DATA APBD</h3></center>
		 	
				<?php foreach ($ld as $key => $l) : ?>
					<form method="post"  action="<?php echo base_url(). 'admin/data_apbd/update' ?>">
						<div class="form-group">
							<label>Judul APBD</label>
							<input type="hidden" name="id_apbd" class="form-control" value="<?php echo $l->id_apbd?>">
							<input type="text" name="jdl_apbd" class="form-control" value="<?php echo $l->jdl_apbd?>">
						</div>

						<div class="form-group">
							<label>Uraian</label>
							<input type="text" name="uraian" class="form-control" value="<?php echo $l->uraian?>">
						</div>

            <div class="form-group">
              <label>tahun</label>
              <input type="text" name="tahun" class="form-control" value="<?php echo $l->tahun?>">
            </div> 

						<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
					</form>

				<?php endforeach;?>
			</div>
		</div>
</div>
</div>