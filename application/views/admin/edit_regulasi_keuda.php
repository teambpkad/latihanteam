<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Data Regulasi Keuda</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Regulasi Keuda administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

		<div class="content-wrapper">
			<div class="container-fluid">
				<center><h3></i>EDIT DATA REGULASI KEUDA</h3></center>
		 	
				<?php foreach ($req_keuda as $key => $kd) : ?>
					<form method="post"  action="<?php echo base_url(). 'admin/data_regulasi_keuda/update' ?>">
						<div class="form-group">
							<label>Kategori Keuda</label>
							<input type="hidden" name="id_keuda" class="form-control" value="<?php echo $kd->id_keuda?>">
							<input type="text" name="ktg_keuda" class="form-control" value="<?php echo $kd->ktg_keuda?>">
						</div>

						<div class="form-group">
							<label>Judul Keuda</label>
							<input type="text" name="nm_keuda" class="form-control" value="<?php echo $kd->nm_keuda?>">
						</div>

						<div class="form-group">
							<label>Uraian Keuda</label>
							<input type="text" name="uraian_keuda" class="form-control" value="<?php echo $kd->uraian_keuda?>">
						</div>

            <div class="form-group">
              <label>Tahun</label>
              <input type="text" name="tahun" class="form-control" value="<?php echo $kd->tahun?>">
            </div>

						<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
					</form>

				<?php endforeach;?>
			</div>
		</div>
</div>
</div>