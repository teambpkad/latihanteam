<?php
	class Model_dashboard_admin extends CI_Model
	{
		function getAll($limit, $offset){
			$this->db->limit($limit);
			$this->db->offset($offset);
			$this->db->order_by('log_id DESC');
			return $this->db->get('tabel_log')->result();
		}
		function countAll(){
			return $this->db->get('tabel_log')->num_rows();
		}
	}
?>