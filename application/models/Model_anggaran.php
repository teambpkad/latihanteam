<?php

class Model_anggaran extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->get('anggaran');
	}

	public function tambah_anggaran($data, $table)
	{
		return $this->db->insert($table, $data);
	}

	public function edit_anggaran($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function get_file($id){
			// $this->db->where('id_keuda', $id);
			// return $this->db->get('keuda');

		$query = $this->db->query("SELECT file FROM anggaran WHERE id_anggaran = '$id' ");
		if ($query->num_rows()>0) {
			$data = $query->result();
			return $data;
		} else {
			return false;
		}
	}

	public function hapus_data($id)
	{
		$this->db->where('id_anggaran', $id);
		return $this->db->delete('anggaran');
	}
}
?>