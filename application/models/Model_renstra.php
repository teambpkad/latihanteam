<?php

class Model_renstra extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->get('renstra');
	}

	public function tambah_renstra($data, $table)
	{
		return $this->db->insert($table, $data);
	}

	public function edit_renstra($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function get_file($id){
			// $this->db->where('id_keuda', $id);
			// return $this->db->get('keuda');

		$query = $this->db->query("SELECT file FROM renstra WHERE id_renstra = '$id' ");
		if ($query->num_rows()>0) {
			$data = $query->result();
			return $data;
		} else {
			return false;
		}
	}

	public function hapus_data($id)
	{
		$this->db->where('id_renstra', $id);
		return $this->db->delete('renstra');
	}
}
?>