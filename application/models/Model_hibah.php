<?php

class Model_hibah extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->get('hibah');
	}

	public function tambah_hibah($data, $table)
	{
		return $this->db->insert($table, $data);
	}

	public function edit_hibah($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function get_file($id){
		$query = $this->db->query("SELECT file FROM hibah WHERE id_hibah = '$id' ");
		if ($query->num_rows()>0) {
			$data = $query->result();
			return $data;
		} else {
			return false;
		}
	}

	public function hapus_data($id)
	{
		$this->db->where('id_hibah', $id);
		return $this->db->delete('hibah');
	}
}
?>