<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View_model extends CI_Model {

	// function that finds the phone by its ID to display in th Bootstrap modal
	public function get_view_data($ViewData)
	{
		$this->db->select('*');
		$this->db->where('id_profile',$ViewData);
		$res2 = $this->db->get('profile');
		return $res2;
	}

}