<?php
	class Model_isi_berita extends CI_Model{

		public function duatable() 
		{
			 $this->db->select('*');
			 $this->db->from('brt_bpkad');
			 $this->db->join('user_bpkad','user_bpkad.id_user=brt_bpkad.id_user');
			 $this->db->join('ktg_brt','ktg_brt.id_ktg_brt=brt_bpkad.id_ktg_brt');
			 $query = $this->db->get();
			 return $query->result();
		}

		public function tampil_data()
		{
			return $this->db->get('brt_bpkad');
		}

		function getAllCategories()
		{
			$this->db->select("*");
			$result = $this->db->get('ktg_brt');
			if($result->num_rows()>0)
			return $result->result();
			else
			return 'empty';
		}	

		function getUser()
		{
			$this->db->select("*");
			$result = $this->db->get('user_bpkad');
			if($result->num_rows()>0)
			return $result->result();
			else
			return 'empty';
		}
	}
?>