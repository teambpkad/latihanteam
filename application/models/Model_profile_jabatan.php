<?php
	class Model_profile_jabatan extends CI_Model
	{
		public function tampil_data(){
		return $this->db->get('profile');
		}	

		public function tambah_profile($data)
		{
			return $this->db->insert('profile', $data);
		}

		public function edit_profile($where, $table)
		{
			return $this->db->get_where($table, $where);
		}

		public function update_data($where, $data, $table)
		{
			$this->db->where($where);
			$this->db->update($table, $data);
		}

		public function get_file($id){
			// $this->db->where('id_keuda', $id);
			// return $this->db->get('keuda');

			$query = $this->db->query("SELECT foto FROM profile WHERE id_profile = '$id' ");
			if ($query->num_rows()>0) {
				$data = $query->result();
				return $data;
			} else {
				return false;
		}
		}

		public function hapus_data($id)
		{
			$this->db->where('id_profile', $id);
			return $this->db->delete('profile');
		}
	}
?>