<?php

class Model_visi extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->get('visi_bpkad');
	}

	public function edit_visi($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

}
?>