<?php

class Model_foto_kegiatan extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->get('foto_kegiatan');
	}

	function getAll($limit, $offset){
			$this->db->limit($limit);
			$this->db->offset($offset);
			$this->db->order_by('id_ft_kegiatan DESC');
			return $this->db->get('foto_kegiatan')->result();
		}
		function countAll(){
			return $this->db->get('foto_kegiatan')->num_rows();
		}

	public function tambah_foto($data)
	{
		return $this->db->insert('foto_kegiatan', $data);
	}

		public function edit_foto($where, $table){
		return $this->db->get_where($table, $where);
		}

		public function update_data($where, $data, $table)
		{
			$this->db->where($where);
			$this->db->update($table, $data);
		}

	public function hapus_data($id)
		{
			$this->db->where('id_ft_kegiatan', $id);
			return $this->db->delete('foto_kegiatan');
		}

		public function get_file($id){
			// $this->db->where('id_keuda', $id);
			// return $this->db->get('keuda');

			$query = $this->db->query("SELECT foto,foto2,foto3 FROM foto_kegiatan WHERE id_ft_kegiatan = '$id' ");
			if ($query->num_rows()>0) {
				$data = $query->result();
				return $data;
			} else {
				return false;
		}
		}
}
?>