<?php
	class Model_regulasi_keuda extends CI_Model
	{
		public function tampil_data()
		{
			return $this->db->get('keuda');
		}


		function getAll($limit, $offset){
			$this->db->limit($limit);
			$this->db->offset($offset);
			$this->db->order_by('id_keuda DESC');
			return $this->db->get('keuda')->result();
		}
		function countAll(){
			return $this->db->get('keuda')->num_rows();
		}

		public function tambah_keuda($data, $table)
		{
			return $this->db->insert($table, $data);
		}

		public function edit_regulasi_keuda($where, $table){
		return $this->db->get_where($table, $where);
		}

		public function update_data($where, $data, $table)
		{
			$this->db->where($where);
			$this->db->update($table, $data);
		}

		public function hapus_data($id)
		{
			$this->db->where('id_keuda', $id);
			return $this->db->delete('keuda');
		}

		public function get_file($id){
			// $this->db->where('id_keuda', $id);
			// return $this->db->get('keuda');

			$query = $this->db->query("SELECT file FROM keuda WHERE id_keuda = '$id' ");
			if ($query->num_rows()>0) {
				$data = $query->result();
				return $data;
			} else {
				return false;
		}
		}

		function get_berita_list($kode){
		$hsl=$this->db->query("SELECT * FROM keuda WHERE id_keuda='$kode'");
		return $hsl;
	}

	}
?>