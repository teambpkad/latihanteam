<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('m_berita');
		$this->load->model('m_carousel');
	}		
	public function index()
	{
		$this->load->view('frontend/_partialsmycss/head.php');
		$this->load->view('frontend/_partialsdashboard/navbar.php');
		$y['data2']=$this->m_carousel->Limit();
		$this->load->view('frontend/_partialsdashboard/carousel.php',$y);
		$this->load->view('frontend/_partialsdashboard/greeting.php');
		$this->load->view('frontend/_partialsdashboard/greeting2.php');
		
		$x['data']=$this->m_berita->Limit();
		$this->load->view('frontend/_partialsdashboard/berita',$x);

		$this->load->view('frontend/_partialsmycss/alamat.php');
		$this->load->view('frontend/_partialsmycss/footer.php');
		$this->load->view('frontend/_partialsmyjs/js.php'); 
	}
}
?>
