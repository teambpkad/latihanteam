<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransaksiNonTunai extends CI_Controller 
    {
        public function index()
        {            
            $this->load->view('frontend/_partialsmycss/head.php');
            $this->load->view('frontend/transaksi_non_tunai/navbar.php');
            $this->load->view('frontend/transaksi_non_tunai/content');
            $this->load->view('frontend/_partialsmycss/alamat.php');
            $this->load->view('frontend/_partialsmycss/footer.php');
            $this->load->view('frontend/_partialsmyjs/js.php'); 
        }
    }
?>
