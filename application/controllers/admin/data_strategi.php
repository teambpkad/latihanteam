<?php
	Class Data_strategi extends CI_Controller{
		public function index()
			{
				$data['strategi'] = $this->model_strategi->tampil_data()->result();
				$this->load->view('templates_admin/header');
				$this->load->view('templates_admin/navbar');
				$this->load->view('templates_admin/link_content/sidebar_strategi');
				$this->load->view('admin/data_strategi',$data);
				$this->load->view('templates_admin/content');
				$this->load->view('templates_admin/footer');
			}

			public function tambah_aksi()
			{
				$isi_strategi = $this->input->post('isi_strategi');

				$data = array (
					'isi_strategi' => $isi_strategi,
				);

				$this->model_strategi->tambah_strategi($data, 'strategi_bpkad');
				helper_log("add", "Tambah Data Strategi");
				redirect('admin/data_strategi');
			}

			public function edit($id)
			{
				$where = array('id_strategi' =>$id);
				$data['strategi'] = $this->model_strategi->edit_strategi($where, 'strategi_bpkad')->result();
				$this->load->view('templates_admin/header');
				$this->load->view('templates_admin/navbar');
				$this->load->view('templates_admin/link_content/sidebar_strategi');
				$this->load->view('admin/data_strategi',$data);
				$this->load->view('templates_admin/content');
				$this->load->view('templates_admin/footer');
			}	

			public function update()
			{
				$id       = $this->input->post('id_strategi');
				$isi_strategi = $this->input->post('isi_strategi');

				$data = array(
					'isi_strategi' => $isi_strategi,
				);
				$where = array (
					'id_strategi' =>$id
				);
				$this->model_strategi->update_data($where, $data, 'strategi_bpkad');
				helper_log("edit", "Edit Data Strategi");
				redirect('admin/data_strategi');
			}

			public function hapus($id)
			{
				$where = array('id_strategi' => $id);
				$this->model_strategi->hapus_data($where, 'strategi_bpkad');
				helper_log("hapus", "Hapus Data Strategi");
				redirect('admin/data_strategi');
			}
	}
?>