<?php
	class Data_blud extends CI_Controller{
		public function index()
		{
			$data['ld'] = $this->model_blud->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_blud');
			$this->load->view('admin/data_blud',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/blud/';
            $config['allowed_types']        = 'pdf';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            $jdl_blud	   = $this->input->post('jdl_blud');
            $uraian        = $this->input->post('uraian');
            $tahun        = $this->input->post('tahun');

            $data = array(
                'jdl_blud'       => $jdl_blud,
                'uraian'     	 => $uraian,
                'file'         	 => $file_name                        
            );

            $this->model_blud->tambah_blud($data, 'blud');
            helper_log("add", "Tambah Data Relasi Blud");
            redirect('admin/data_blud');
		}

		public function edit($id)
		{
			$where = array('id_blud' =>$id);
			$data['ld'] = $this->model_blud->edit_blud($where, 'blud')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_blud');
			$this->load->view('admin/edit_blud',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id          = $this->input->post('id_blud');
			$jdl_blud = $this->input->post('jdl_blud');
			$uraian      = $this->input->post('uraian');

			$data = array(
				'jdl_blud' => $jdl_blud,
				'uraian'      => $uraian
			);
			$where = array (
				'id_blud' =>$id
			);
			$this->model_blud->update_data($where, $data, 'blud');
			helper_log("edit", "Edit Data Relasi Blud");
			redirect('admin/data_blud');
		}

		public function hapus($id)
		{
			$file = $this->model_blud->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/blud/".$nama_file);
            $hasil = $this->model_blud->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data Relasi Blud");
            redirect('admin/data_blud');
		}
	}
?>