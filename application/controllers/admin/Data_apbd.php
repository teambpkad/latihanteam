<?php
	class Data_apbd extends CI_Controller{
		public function index()
		{
			$data['ld'] = $this->model_apbd->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_apbd');
			$this->load->view('admin/data_apbd',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}
 
		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/apbd/';
            $config['allowed_types']        = 'pdf';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            
            $jdl_apbd	   = $this->input->post('jdl_apbd');
            $jenis_apbd    = $this->input->post('jenis_apbd');
            $uraian        = $this->input->post('uraian');
            $tahun         = $this->input->post('tahun');

            $data = array(
                'jdl_apbd'       => $jdl_apbd,
                'jenis_apbd'	 => $jenis_apbd,
                'uraian'     	 => $uraian,
                'tahun'     	 => $tahun,
                'file'         	 => $file_name                        
            );

            $this->model_apbd->tambah_apbd($data, 'apbd');
            helper_log("add", "Tambah Data APBD");
            redirect('admin/data_apbd');
		}

		public function edit($id)
		{
			$where = array('id_apbd' =>$id);
			$data['ld'] = $this->model_apbd->edit_apbd($where, 'apbd')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_apbd');
			$this->load->view('admin/edit_apbd',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id          = $this->input->post('id_apbd');
			$jdl_apbd = $this->input->post('jdl_apbd');
			$uraian      = $this->input->post('uraian');
			$tahun      = $this->input->post('tahun');

			$data = array(
				'jdl_apbd' => $jdl_apbd,
				'uraian'      => $uraian,
				'tahun'      => $tahun
			);
			$where = array (
				'id_apbd' =>$id
			);
			$this->model_apbd->update_data($where, $data, 'apbd');
			helper_log("edit", "Edit Data APBD");
			redirect('admin/data_apbd');
		}

		public function hapus($id)
		{
			$file = $this->model_apbd->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/apbd/".$nama_file);
            $hasil = $this->model_apbd->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data APBD");
            redirect('admin/data_apbd');
		}
	}
?>