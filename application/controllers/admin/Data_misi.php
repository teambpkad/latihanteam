<?php
	class Data_misi extends CI_Controller{
			public function index()
			{
				$data['misi'] = $this->model_misi->tampil_data()->result();
				$this->load->view('templates_admin/header');
				$this->load->view('templates_admin/navbar');
				$this->load->view('templates_admin/link_content/sidebar_misi');
				$this->load->view('admin/data_misi',$data);
				$this->load->view('templates_admin/content'); 
				$this->load->view('templates_admin/footer');
			}

			public function tambah_aksi()
			{
				$isi_misi = $this->input->post('isi_misi');

				$data = array (
					'isi_misi' => $isi_misi,
				);

			$this->model_misi->tambah_misi($data, 'misi_bpkad');
			helper_log("add", "Tambah Data Misi");
			redirect('admin/data_misi');
			}


			public function edit($id)
			{
				$where = array('id_misi' =>$id);
				$data['misi'] = $this->model_misi->edit_misi($where, 'misi_bpkad')->result();
				$this->load->view('templates_admin/header');
				$this->load->view('templates_admin/navbar');
				$this->load->view('templates_admin/link_content/sidebar_misi');
				$this->load->view('admin/edit_misi',$data);
				$this->load->view('templates_admin/content'); 
				$this->load->view('templates_admin/footer');
			}	

			public function update()
			{
				$id       = $this->input->post('id_misi');
				$isi_misi = $this->input->post('isi_misi');

				$data = array(
					'isi_misi' => $isi_misi,
				);
				$where = array (
					'id_misi' =>$id
				);
				$this->model_misi->update_data($where, $data, 'misi_bpkad');
				helper_log("edit", "Edit Data Misi");
				redirect('admin/data_misi');
			}

			public function hapus($id)
			{
				$where = array('id_misi' => $id);
				$this->model_misi->hapus_data($where, 'misi_bpkad');
				helper_log("hapus", "Hapus Data Misi");
				redirect('admin/data_misi');
			}
		}
?>