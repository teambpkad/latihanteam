<?php
	class Data_lkpj extends CI_Controller{
		public function index()
		{
			$data['lk'] = $this->model_lkpj->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_lkpj');
			$this->load->view('admin/data_lkpj',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/lkpj/';
            $config['allowed_types']        = 'pdf';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            $jdl_lkpj	   = $this->input->post('jdl_lkpj');
            $uraian        = $this->input->post('uraian');
            $bagian        = $this->input->post('bagian');
            $tahun        = $this->input->post('tahun');

            $data = array(
                'jdl_lkpj'       => $jdl_lkpj,
                'uraian'     	 => $uraian,
                'bagian'     	 => $bagian,
                'tahun'     	 => $tahun,
                'file'         	 => $file_name                        
            );

            $this->model_lkpj->tambah_lkpj($data, 'lkpj');
            helper_log("add", "Tambah Data LKPJ");
            redirect('admin/data_lkpj');
		}

		public function edit($id)
		{
			$where = array('id_lkpj' =>$id);
			$data['lk'] = $this->model_lkpj->edit_lkpj($where, 'lkpj')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_lkpj');
			$this->load->view('admin/edit_lkpj',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id          = $this->input->post('id_lkpj');
			$jdl_lkpj = $this->input->post('jdl_lkpj');
			$uraian      = $this->input->post('uraian');
			$bagian      = $this->input->post('bagian');
			$tahun      = $this->input->post('tahun');

			$data = array(
				'jdl_lkpj' => $jdl_lkpj,
				'uraian'      => $uraian,
				'bagian'      => $bagian,
				'tahun'      => $tahun
			);
			$where = array (
				'id_lkpj' =>$id
			);
			$this->model_lkpj->update_data($where, $data, 'lkpj');
			helper_log("edit", "Edit Data LKPJ");
			redirect('admin/data_lkpj');
		}

		public function hapus($id)
		{
			$file = $this->model_lkpj->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/lkpj/".$nama_file);
            $hasil = $this->model_lkpj->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data LKPJ");
            redirect('admin/data_lkpj');
		}
	}
?>