<?php
	class Data_foto_kegiatan extends CI_Controller
	{
		public function __construct() 
		{
	        parent::__construct();
            $this->load->library('pagination');
	        $this->load->model('model_foto_kegiatan');
    	}

		public function index()
		{
			$data['ft_kegiatan'] = $this->model_foto_kegiatan->tampil_data()->result();
            $this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/link_content/sidebar_galeri');
            $this->load->view('admin/data_foto_kegiatan',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
        }

        public function tambah_aksi()
        {
            $config['upload_path'] = './images/';
            $config['allowed_types']= 'jpg|png|jpeg';
            $config['image_library'] = 'GD2';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $name_file1 = "";
            $name_file2 = "";
            $name_file3 = "";

            if ( ! $this->upload->do_upload('foto'))
            {
                    $error = array('error' => $this->upload->display_errors());

                    //$this->load->view('upload_form', $error);
            }
            else
            {
                $name_file1  =  $this->upload->data('file_name');

                //$this->load->view('upload_success', $name_file1);
                   //$name_file1  =  $this->upload->data('file_name');
            }

            if ( ! $this->upload->do_upload('foto2'))
            {
                    $error = array('error' => $this->upload->display_errors());

                    //$this->load->view('upload_form', $error);
            }
            else
            {
                $name_file2  =  $this->upload->data('file_name');

                //$this->load->view('upload_success', $name_file1);
                   //$name_file1  =  $this->upload->data('file_name');
            }

            if ( ! $this->upload->do_upload('foto3'))
            {
                    $error = array('error' => $this->upload->display_errors());

                    //$this->load->view('upload_form', $error);
            }
            else
            {
                $name_file3 =  $this->upload->data('file_name');

                //$this->load->view('upload_success', $name_file1);
                   //$name_file1  =  $this->upload->data('file_name');
            }

            // if ($this->upload->do_upload('foto')) {
            //     $name_file1  =  $this->upload->data('file_name');
            // }

            // if ($this->upload->do_upload('foto2')) {
            //     $name_file2  =  $this->upload->data('file_name');

            // }
 
            // if ($this->upload->do_upload('foto3')) {
            //     $name_file3  = $this->upload->data('file_name');
            // }

            $jdl_kegiatan       = $this->input->post('jdl_kegiatan');
            $keterangan         = $this->input->post('keterangan');
            $status             = $this->input->post('status');
            $tgl                = $this->input->post('tgl_kegiatan');
        
            $data = array(
                'jdl_kegiatan'   => $jdl_kegiatan,
                'keterangan'     => $keterangan,
                'foto'           => $name_file1,                        
                'foto2'          => $name_file2,                        
                'foto3'          => $name_file3,                        
                'status'         => $status,
                'tgl_kegiatan'   => $tgl                       
            );
            $tambah = $this->model_foto_kegiatan->tambah_foto($data);
            redirect('admin/data_foto_kegiatan');           

        }


        public function edit($id)
        {
            $where = array('id_ft_kegiatan' =>$id);
            $data['ft_kegiatan'] = $this->model_foto_kegiatan->edit_foto($where, 'foto_kegiatan')->result();
            $this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/link_content/sidebar_galeri');
            $this->load->view('admin/edit_foto_kegiatan',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
        }

        public function update()
        {
            $id              = $this->input->post('id_ft_kegiatan');
            $judul       = $this->input->post('jdl_kegiatan');
            $keterangan        = $this->input->post('keterangan');
            $status             = $this->input->post('status');

            $data = array(
                'jdl_kegiatan'       => $judul,
                'keterangan'        => $keterangan,
                'status'            => $status    
            );
            $where = array (
                'id_ft_kegiatan' =>$id
            );
            $this->model_foto_kegiatan->update_data($where, $data, 'foto_kegiatan');
            helper_log("edit", "Edit Data Foto Kegiatan");
            redirect('admin/data_foto_kegiatan');
        }

        public function hapus($id)
        {
            
            $foto = $this->model_foto_kegiatan->get_file($id);
            foreach($foto as $ft){
                $nama_file1 = $ft->foto;
                $nama_file2 = $ft->foto2;
                $nama_file3 = $ft->foto3;
            }
            unlink("./images/".$nama_file1);
            unlink("./images/".$nama_file2);
            unlink("./images/".$nama_file3);
            $hasil = $this->model_foto_kegiatan->hapus_data($id);
            helper_log("hapus", "Hapus Data Foto Kegiatan");
            redirect('admin/data_foto_kegiatan');

        }
	}
?>	