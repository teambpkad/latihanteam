<?php
	class Data_hibah extends CI_Controller{
		public function index()
		{
			$data['h'] = $this->model_hibah->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_hibah');
			$this->load->view('admin/data_hibah',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/hibah/';
            $config['allowed_types']        = 'pdf';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            $jdl_hibah	   = $this->input->post('jdl_hibah');
            $uraian        = $this->input->post('uraian');
            $tahun        = $this->input->post('tahun');

            $data = array(
                'jdl_hibah'       => $jdl_hibah,
                'uraian'     	 => $uraian,
                'tahun'     	 => $tahun,
                'file'         	 => $file_name                        
            );

            $this->model_hibah->tambah_hibah($data, 'hibah');
            helper_log("add", "Tambah Data HIBAH");
            redirect('admin/data_hibah');
		}

		public function edit($id)
		{
			$where = array('id_hibah' =>$id);
			$data['h'] = $this->model_hibah->edit_hibah($where, 'hibah')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_hibah');
			$this->load->view('admin/edit_hibah',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id          = $this->input->post('id_hibah');
			$jdl_hibah = $this->input->post('jdl_hibah');
			$uraian      = $this->input->post('uraian');
			$tahun      = $this->input->post('tahun');

			$data = array(
				'jdl_hibah' => $jdl_hibah,
				'uraian'      => $uraian,
				'tahun'      => $tahun
			);
			$where = array (
				'id_hibah' =>$id
			);
			$this->model_hibah->update_data($where, $data, 'hibah');
			helper_log("edit", "Edit Data HIBAH");
			redirect('admin/data_hibah');
		}

		public function hapus($id)
		{
			$file = $this->model_hibah->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/hibah/".$nama_file);
            $hasil = $this->model_hibah->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data HIBAH");
            redirect('admin/data_hibah');
		}
	}
?>