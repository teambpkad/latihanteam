<?php
	class Data_profile_jabatan extends CI_Controller{
		public function index()
		{
			$data['profile'] = $this->model_profile_jabatan->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_profil');
			$this->load->view('admin/data_profile_jabatan',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
        {
            $config['upload_path'] = './assets/images/profil/';
            $config['allowed_types']= 'jpg|png|jpeg';
            $config['max_size'] = '100000';
            $config['image_library'] = 'GD2';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $name_file1 = "";
             if ( ! $this->upload->do_upload('foto'))
            {
                    $error = array('error' => $this->upload->display_errors());

                    $this->load->view('upload_form', $error);
            }
            else
            {
                $name_file1  =  $this->upload->data('file_name');

                //$this->load->view('upload_success', $name_file1);
                   //$name_file1  =  $this->upload->data('file_name');
            }

            
            // if ($this->upload->do_upload('foto')) {
            //     $name_file1  =  $this->upload->data('file_name');
            // }

            $nm_profile       = $this->input->post('nm_profile');
            $nip         	  = $this->input->post('nip');
            $jabatan          = $this->input->post('jabatan');
            $pangkat		  = $this->input->post('pangkat');
        
            $data = array(
                'nm_profile'   => $nm_profile,
                'nip'          => $nip,
                'jabatan'      => $jabatan,  
                'foto'         => $name_file1,   
                'pangkat'	   => $pangkat       
            );
            $tambah = $this->model_profile_jabatan->tambah_profile($data);
			//helper_log("add", "Tambah Data Profile");
            redirect('admin/data_profile_jabatan');     
        }

        public function edit($id)
		{
			$where = array('id_profile' =>$id);
			$data['profile'] = $this->model_profile_jabatan->edit_profile($where, 'profile')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_profil');
			$this->load->view('admin/edit_profile_jabatan',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
        {
            $id         = $this->input->post('id_profile');
            $nm_profile = $this->input->post('nm_profile');
            $nip        = $this->input->post('nip');
            $jabatan    = $this->input->post('jabatan');
            $foto_lama  = $this->input->post('old_foto');
            // um tuka jadi ko mudah2an bisa
            $foto_baru  = $_FILES['new_foto']['name'];
 
            $config['upload_path'] = './assets/images/profil/';
                $config['allowed_types']= 'jpg|png|jpeg|JPG|PNG';
                $config['max_size'] = '10240';
                $config['image_library'] = 'GD2';
                $this->load->library('upload', $config);

            if($foto_baru =''){
                if($this->upload->do_upload('old_foto')){
                    $name_file1 = "";
                    $name_file1  =  $this->upload->data('file_name');
                }
               
                
            }else{
                 
                if($this->upload->do_upload('new_foto')){
                    $name_file1 = "";
                    $name_file1  =  $this->upload->data('file_name');
                }
                unlink("./assets/images/profil/".$foto_lama);
            }
            $data = array(
                    'nm_profile' => $nm_profile,
                    'nip'        => $nip,
                    'jabatan'    => $jabatan,
                    'foto'       => $name_file1 
                );
                
            $where = array (
                'id_profile' =>$id
            );
            $this->model_profile_jabatan->update_data($where, $data, 'profile');
            
            redirect('admin/data_profile_jabatan');
              
        }

		public function hapus($id)
		{
			$foto = $this->model_profile_jabatan->get_file($id);
            foreach($foto as $ft){
                $nama_file = $ft->foto;
            }
            unlink("./assets/images/profil/".$nama_file);
            $hasil = $this->model_profile_jabatan->hapus_data($id);echo $foto;
            //helper_log("hapus", "Hapus Data Regulasi Keuda");
            redirect('admin/data_profile_jabatan');
		}
	}
?>
