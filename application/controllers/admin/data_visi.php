<?php
	class Data_visi extends CI_Controller{
			public function index()
			{
				$data['visi'] = $this->model_visi->tampil_data()->result();
				$this->load->view('templates_admin/header');
				$this->load->view('templates_admin/navbar');
				$this->load->view('templates_admin/link_content/sidebar_visi');
				$this->load->view('admin/data_visi',$data);
				$this->load->view('templates_admin/content');
				$this->load->view('templates_admin/footer');
			}

			public function tambah_aksi()
			{
				$isi_visi = $this->input->post('isi_visi');

				$data = array (
					'isi_visi' => $isi_visi,
				);

				$this->model_visi->tambah_visi($data, 'visi_bpkad');
				helper_log("add", "Tambah Data Visi");
				redirect('admin/data_visi/index');
			}


			public function edit($id)
			{
				$where = array('id_visi' =>$id);
				$data['visi'] = $this->model_visi->edit_visi($where, 'visi_bpkad')->result();
				$this->load->view('templates_admin/header');
				$this->load->view('templates_admin/navbar');
				$this->load->view('templates_admin/link_content/sidebar_visi');
				$this->load->view('admin/edit_visi',$data);
				$this->load->view('templates_admin/content');
				$this->load->view('templates_admin/footer');
			}	

			public function update()
			{
				$id       = $this->input->post('id_visi');
				$isi_visi = $this->input->post('isi_visi');

				$data = array(
					'isi_visi' => $isi_visi,
				);
				$where = array (
					'id_visi' =>$id
				);
				$this->model_visi->update_data($where, $data, 'visi_bpkad');
				helper_log("edit", "Edit Data Visi");
				redirect('admin/data_visi/index');
			}


		}
?>