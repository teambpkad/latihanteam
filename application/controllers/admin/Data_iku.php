<?php
	class Data_iku extends CI_Controller{
		public function index()
		{
			$data['ld'] = $this->model_iku->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_anggaran');
			$this->load->view('admin/data_iku',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/iku/';
            $config['allowed_types']        = 'pdf';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            $jdl_iku	   = $this->input->post('jdl_iku');
            $uraian       	   = $this->input->post('uraian');

            $data = array(
                'jdl_iku'        => $jdl_iku,
                'uraian'     	 => $uraian,
                'file'         	 => $file_name                        
            );

            $this->model_iku->tambah_iku($data, 'iku');
            //helper_log("add", "Tambah Data Relasi Anggaran");
            redirect('admin/data_iku');
		}

		public function edit($id)
		{
			$where = array('id_anggaran' =>$id);
			$data['ld'] = $this->model_anggaran->edit_anggaran($where, 'anggaran')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_anggaran');
			$this->load->view('admin/edit_anggaran',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id          = $this->input->post('id_anggaran');
			$jdl_anggaran = $this->input->post('jdl_anggaran');
			$uraian      = $this->input->post('uraian');
			$tahun      = $this->input->post('tahun');

			$data = array(
				'jdl_anggaran' => $jdl_anggaran,
				'uraian'      => $uraian,
				'tahun'      => $tahun
			);
			$where = array (
				'id_anggaran' =>$id
			);
			$this->model_anggaran->update_data($where, $data, 'anggaran');
			helper_log("edit", "Edit Data Relasi Anggaran");
			redirect('admin/data_anggaran');
		}

		public function hapus($id)
		{
			$file = $this->model_iku->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/iku/".$nama_file);
            $hasil = $this->model_iku->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data Relasi Anggaran");
            redirect('admin/data_iku');
		}
	}
?>