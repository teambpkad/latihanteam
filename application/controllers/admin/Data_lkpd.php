<?php
	class Data_lkpd extends CI_Controller{
		public function index()
		{
			$data['ld'] = $this->model_lkpd->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_lkpd');
			$this->load->view('admin/data_lkpd',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/lkpd/';
            $config['allowed_types']        = 'pdf';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            $jdl_lkpd	   = $this->input->post('jdl_lkpd');
            $uraian        = $this->input->post('uraian');
            $bagian        = $this->input->post('bagian');
            $tahun        = $this->input->post('tahun');

            $data = array(
                'jdl_lkpd'       => $jdl_lkpd,
                'uraian'     	 => $uraian,
                'bagian'     	 => $bagian,
                'tahun'     	 => $tahun,
                'file'         	 => $file_name                        
            );

            $this->model_lkpd->tambah_lkpd($data, 'lkpd');
            helper_log("add", "Tambah Data LKPD");
            redirect('admin/data_lkpd');
		}

		public function edit($id)
		{
			$where = array('id_lkpd' =>$id);
			$data['ld'] = $this->model_lkpd->edit_lkpd($where, 'lkpd')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_lkpd');
			$this->load->view('admin/edit_lkpd',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id          = $this->input->post('id_lkpd');
			$jdl_lkpd = $this->input->post('jdl_lkpd');
			$uraian      = $this->input->post('uraian');
			$bagian      = $this->input->post('bagian');
			$tahun      = $this->input->post('tahun');

			$data = array(
				'jdl_lkpd' => $jdl_lkpd,
				'uraian'      => $uraian,
				'bagian'      => $bagian,
				'tahun'      => $tahun
			);
			$where = array (
				'id_lkpd' =>$id
			);
			$this->model_lkpd->update_data($where, $data, 'lkpd');
			helper_log("edit", "Edit Data LKPd");
			redirect('admin/data_lkpd');
		}

		public function hapus($id)
		{
			$file = $this->model_lkpd->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/lkpd/".$nama_file);
            $hasil = $this->model_lkpd->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data LKPd");
            redirect('admin/data_lkpd');
		}
	}
?>