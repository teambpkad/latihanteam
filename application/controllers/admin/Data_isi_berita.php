<?php
	class Data_isi_berita extends CI_Controller{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('model_isi_berita', '', TRUE);
			$this->load->helper(array('form', 'url'));
		}

		public function index()
		{
			$data['join2'] = "Join CodeIgniter"; 
			$data['isi_berita'] = $this->model_isi_berita->duatable(); 
  			//$this->load->view('nong',$data);    
			//$data['isi_berita'] = $this->model_isi_berita->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('admin/data_isi_berita',$data);
			$this->load->view('templates_admin/sidebar');
			$this->load->view('templates_admin/footer');
		}
	}
?>