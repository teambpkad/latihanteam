<?php
	class Dashboard_admin extends CI_Controller{

		public function __construct() 
		{
	        parent::__construct();
            $this->load->library('pagination');
	        $this->load->model('model_log_aktivitas');
    	}

		public function index($offset=0)
		{
            $this->load->library('pagination');
            $config['base_url'] = site_url('admin/dashboard_admin/index');
            $config['total_rows'] = $this->model_log_aktivitas->countAll();
            $config['per_page'] = 10;
            $config['first_link']       = 'First';
            $config['last_link']        = 'Last';
            $config['next_link']        = 'Next';
            $config['prev_link']        = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            $this->pagination->initialize($config);
            $data['log'] = $this->model_log_aktivitas->getAll($config['per_page'], $offset);
			//$data['req_keuda'] = $this->model_regulasi_keuda->tampil_data()->result();
            $this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/sidebar');
            $this->load->view('admin/dashboard_admin',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
  //       }
		// public function __construct() 
		// {
	 //        parent::__construct();
  //           $this->load->library('pagination');
	 //        $this->load->model('model_log_aktivitas');
  //   	}
		// public function index($offset=0){

		// 	$this->load->library('pagination');
  //           $config['base_url'] = site_url('admin/dashboard_admin/index');
  //           $config['total_rows'] = $this->model_log_aktivitas->countAll();
  //           $config['per_page'] = 5;
  //           $config['first_link']       = 'First';
  //           $config['last_link']        = 'Last';
  //           $config['next_link']        = 'Next';
  //           $config['prev_link']        = 'Prev';
  //           $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
  //           $config['full_tag_close']   = '</ul></nav></div>';
  //           $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
  //           $config['num_tag_close']    = '</span></li>';
  //           $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
  //           $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
  //           $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
  //           $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
  //           $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
  //           $config['prev_tagl_close']  = '</span>Next</li>';
  //           $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
  //           $config['first_tagl_close'] = '</span></li>';
  //           $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
  //           $config['last_tagl_close']  = '</span></li>';
  //           $this->pagination->initialize($config);
  //           $data['log'] = $this->model_log_aktivitas->getAll($config['per_page'], $offset);

		// 	$this->load->view('templates_admin/header');
		// 	$this->load->view('templates_admin/navbar');
		// 	$this->load->view('templates_admin/sidebar');
		// 	$this->load->view('admin/dashboard_admin',$data);
		// 	$this->load->view('templates_admin/content');
		// 	$this->load->view('templates_admin/footer');

		// }
        }
	}
?>