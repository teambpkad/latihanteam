<?php
	class Data_anggaran extends CI_Controller{
		public function index()
		{
			$data['ld'] = $this->model_anggaran->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_anggaran');
			$this->load->view('admin/data_anggaran',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/anggaran/';
            $config['allowed_types']        = 'pdf';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            $jdl_anggaran	   = $this->input->post('jdl_anggaran');
            $uraian        = $this->input->post('uraian');
            $tahun        = $this->input->post('tahun');

            $data = array(
                'jdl_anggaran'       => $jdl_anggaran,
                'uraian'     	 => $uraian,
                'tahun'     	 => $tahun,
                'file'         	 => $file_name                        
            );

            $this->model_anggaran->tambah_anggaran($data, 'anggaran');
            helper_log("add", "Tambah Data Relasi Anggaran");
            redirect('admin/data_anggaran');
		}

		public function edit($id)
		{
			$where = array('id_anggaran' =>$id);
			$data['ld'] = $this->model_anggaran->edit_anggaran($where, 'anggaran')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_anggaran');
			$this->load->view('admin/edit_anggaran',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id          = $this->input->post('id_anggaran');
			$jdl_anggaran = $this->input->post('jdl_anggaran');
			$uraian      = $this->input->post('uraian');
			$tahun      = $this->input->post('tahun');

			$data = array(
				'jdl_anggaran' => $jdl_anggaran,
				'uraian'      => $uraian,
				'tahun'      => $tahun
			);
			$where = array (
				'id_anggaran' =>$id
			);
			$this->model_anggaran->update_data($where, $data, 'anggaran');
			helper_log("edit", "Edit Data Relasi Anggaran");
			redirect('admin/data_anggaran');
		}

		public function hapus($id)
		{
			$file = $this->model_anggaran->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/anggaran/".$nama_file);
            $hasil = $this->model_anggaran->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data Relasi Anggaran");
            redirect('admin/data_anggaran');
		}
	}
?>