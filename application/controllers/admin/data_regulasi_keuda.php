<?php
	class Data_regulasi_keuda extends CI_Controller
	{
		public function __construct() 
		{
	        parent::__construct();
            $this->load->library('pagination');
	        $this->load->model('model_regulasi_keuda');
    	}

		public function index()
		{
			$data['req_keuda'] = $this->model_regulasi_keuda->tampil_data()->result();
            $this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/link_content/sidebar_regulasi');
            $this->load->view('admin/data_regulasi_keuda',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
        }
		

        public function tambah_aksi()
        {

            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'pdf';


            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
                $ktg_keuda       = $this->input->post('ktg_keuda');
                $nm_keuda        = $this->input->post('nm_keuda');
                $uraian_keuda    = $this->input->post('uraian_keuda');
                $tahun    = $this->input->post('tahun');
                

                $data = array(
                    'ktg_keuda'    => $ktg_keuda,
                    'nm_keuda'     => $nm_keuda,
                    'uraian_keuda' => $uraian_keuda,
                    'tahun' => $tahun,
                    'file'         => $file_name                        
                );

                $this->model_regulasi_keuda->tambah_keuda($data, 'keuda');
                helper_log("add", "Tambah Data Regulasi Keuda");
                redirect('admin/data_regulasi_keuda');
            
        }

        public function edit($id)
        {
            $where = array('id_keuda' =>$id);
            $data['req_keuda'] = $this->model_regulasi_keuda->edit_regulasi_keuda($where, 'keuda')->result();
            $this->load->view('templates_admin/header');
            $this->load->view('templates_admin/navbar');
            $this->load->view('templates_admin/link_content/sidebar_regulasi');
            $this->load->view('admin/edit_regulasi_keuda',$data);
            $this->load->view('templates_admin/content');
            $this->load->view('templates_admin/footer');
        }

        public function update()
        {
            
            $id              = $this->input->post('id_keuda');
            $ktg_keuda       = $this->input->post('ktg_keuda');
            $nm_keuda        = $this->input->post('nm_keuda');
            $uraian_keuda    = $this->input->post('uraian_keuda');
            $tahun    = $this->input->post('tahun');

            $data = array(
                'ktg_keuda'       => $ktg_keuda,
                'nm_keuda'        => $nm_keuda,
                //'pwd'      => md5($pwd),
                'uraian_keuda'    => $uraian_keuda,
                'tahun'    => $tahun,
            );
            $where = array (
                'id_keuda' =>$id
            );
            $this->model_regulasi_keuda->update_data($where, $data, 'keuda');
            helper_log("edit", "Edit Data Regulasi Keuda");
            redirect('admin/data_regulasi_keuda');
        }

        public function hapus($id)
        {
            
            $file = $this->model_regulasi_keuda->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/".$nama_file);
            $hasil = $this->model_regulasi_keuda->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data Regulasi Keuda");
            redirect('admin/data_regulasi_keuda');

        }
        

	}
?>