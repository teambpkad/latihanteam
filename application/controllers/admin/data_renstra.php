<?php
	class Data_renstra extends CI_Controller{
		public function index()
		{
			$data['ren'] = $this->model_renstra->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_renstra');
			$this->load->view('admin/data_renstra',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$config['upload_path']          = './uploads/renstra/';
            $config['allowed_types']        = 'pdf';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file'))
            {
                $file_name  =  $this->upload->data('file_name');
            }
            $jdl_renstra   = $this->input->post('jdl_renstra');
            $uraian        = $this->input->post('uraian');
            $tahun        = $this->input->post('tahun');
            


            $data = array(
                'jdl_renstra'    => $jdl_renstra,
                'uraian'     	 => $uraian,
                'tahun'     	 => $tahun,
                'file'         	 => $file_name                        
            );

            $this->model_renstra->tambah_renstra($data, 'renstra');
            helper_log("add", "Tambah Data Renstra");
            redirect('admin/data_renstra');
		}

		public function edit($id)
		{
			$where = array('id_renstra' =>$id);
			$data['ren'] = $this->model_renstra->edit_renstra($where, 'renstra')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/link_content/sidebar_renstra');
			$this->load->view('admin/edit_renstra',$data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id          = $this->input->post('id_renstra');
			$jdl_renstra = $this->input->post('jdl_renstra');
			$uraian      = $this->input->post('uraian');
			$tahun      = $this->input->post('tahun');

			$data = array(
				'jdl_renstra' => $jdl_renstra,
				'uraian'      => $uraian,
				'tahun'      => $tahun
			);
			$where = array (
				'id_renstra' =>$id
			);
			$this->model_renstra->update_data($where, $data, 'renstra');
			helper_log("edit", "Edit Data Renstra");
			redirect('admin/data_renstra');
		}

		public function hapus($id)
		{
			$file = $this->model_renstra->get_file($id);
            foreach($file as $ft){
                $nama_file = $ft->file;
            }
            unlink("./uploads/renstra/".$nama_file);
            $hasil = $this->model_renstra->hapus_data($id);echo $file;
            helper_log("hapus", "Hapus Data Renstra");
            redirect('admin/data_renstra');
		}
	}
?>