<?php
	class Data_kebijakan extends CI_Controller
	{
		
		public function index()
			{
				$data['kebijakan'] = $this->model_kebijakan->tampil_data()->result();
				$this->load->view('templates_admin/header');
				$this->load->view('templates_admin/navbar');
				$this->load->view('templates_admin/link_content/sidebar_kebijakan');
				$this->load->view('admin/data_kebijakan',$data);
				$this->load->view('templates_admin/content');
				$this->load->view('templates_admin/footer');
			}

			public function tambah_aksi()
			{
				$isi_kebijakan = $this->input->post('isi_kebijakan');

				$data = array (
					'isi_kebijakan' => $isi_kebijakan,
				);

			$this->model_kebijakan->tambah_kebijakan($data, 'kebijakan_bpkad');
			helper_log("add", "Tambah Data Kebijakan");
			redirect('admin/data_kebijakan');
			}


			public function edit($id)
			{
				$where = array('id_kebijakan' =>$id);
				$data['kebijakan'] = $this->model_kebijakan->edit_kebijakan($where, 'kebijakan_bpkad')->result();
				$this->load->view('templates_admin/header');
				$this->load->view('templates_admin/navbar');
				$this->load->view('templates_admin/link_content/sidebar_kebijakan');
				$this->load->view('admin/edit_kebijakan',$data);
				$this->load->view('templates_admin/content');
				$this->load->view('templates_admin/footer');
			}	

			public function update()
			{
				$id       = $this->input->post('id_kebijakan');
				$isi_kebijakan = $this->input->post('isi_kebijakan');

				$data = array(
					'isi_kebijakan' => $isi_kebijakan,
				);
				$where = array (
					'id_kebijakan' =>$id
				);
				$this->model_kebijakan->update_data($where, $data, 'kebijakan_bpkad');
				helper_log("hapus", "Edit Data Kebijakan");
				redirect('admin/data_kebijakan');
			}

			public function hapus($id)
			{
				$where = array('id_misi' => $id);
				$this->model_misi->hapus_data($where, 'misi_bpkad');
				helper_log("hapus", "Hapus Data Kebijakan");
				redirect('admin/data_misi');
			}
	}
?>