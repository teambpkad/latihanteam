<?php
	/**
	 * 
	 */
	class Data_kategori_berita extends CI_Controller
	{
		
		public function index()
		{
			$data['kategori_berita'] = $this->model_kategori_berita->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('admin/data_kategori_berita',$data);
			$this->load->view('templates_admin/sidebar');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$nm_ktg          = $this->input->post('nm_ktg');
			$keterangan      = $this->input->post('keterangan');

			$data = array (
				'nm_ktg' 		  => $nm_ktg,
				'keterangan'      => $keterangan,
			);

		$this->model_kategori_berita->tambah_kategori_berita($data, 'ktg_brt');
		redirect('admin/data_kategori_berita');
		}

		public function edit($id)
		{
			$where = array('id_ktg_brt' =>$id);
			$data['kategori_berita'] = $this->model_kategori_berita->edit_kategori_berita($where, 'ktg_brt')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('admin/edit_kategori_berita',$data);
			$this->load->view('templates_admin/sidebar');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id      		 = $this->input->post('id_ktg_brt');
			$nm_ktg 		 = $this->input->post('nm_ktg');
			$keterangan      = $this->input->post('keterangan');

			$data = array(
				'nm_ktg' 		  => $nm_ktg,
				'keterangan'      => ($keterangan),
			);
			$where = array (
				'id_ktg_brt' =>$id
			);
			$this->model_kategori_berita->update_data($where, $data, 'ktg_brt');
			redirect('admin/data_kategori_berita');
		}

		public function hapus($id)
		{
			$where = array('id_ktg_brt' => $id);
			$this->model_kategori_berita->hapus_data($where, 'ktg_brt');
			redirect('admin/data_kategori_berita');
		}
	}
?>