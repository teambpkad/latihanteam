<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usahamikro extends CI_Controller 
    {
        public function index()
        {           
            $this->load->view('frontend/_partialsmycss/head.php');
            $this->load->view('frontend/dana_bergulir/persyaratan_kriteria_usahamikro/navbar.php');
            $this->load->view('frontend/dana_bergulir/persyaratan_kriteria_usahamikro/content');
            $this->load->view('frontend/_partialsmycss/alamat.php');
            $this->load->view('frontend/_partialsmycss/footer.php');
            $this->load->view('frontend/_partialsmyjs/js.php'); 
        }
    }
?>
