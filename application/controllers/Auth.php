<?php 
	class Auth extends CI_Controller{
		public function __construct() 
		{
	        parent::__construct();
            
	        $this->load->model('model_log_aktivitas');
    	}

		public function login()
		{

			$this->load->view('login');
			$valid = $this->form_validation;
			$username = $this->input->post('username');
			$password = $this->input->post('pwd');
			$passwordx = md5($password);

			
			$valid->set_rules('username','username','required');
			$valid->set_rules('pwd','pwd','required');
			if($valid->run()) {
				//save log aktivitas
				$tgl = $this->input->post('username');
				$pwd      = $this->input->post('pwd');
				$level    = $this->input->post('level');
				$bagian   = $this->input->post('bagian');

				$data = array (
					'username' => $username,
					//'pwd'      => md5($pwd),
					'pwd'      => $pwd,
					'level'    => $level,
					'bagian'   => $bagian
				);

		//$this->model_user->tambah_user($data, 'user_bpkad');
			$this->model_auth->login($username,$passwordx, base_url('admin/dashboard_admin'), base_url('auth/login'));
			helper_log("login", "Login");
			}
// End fungsi login
		}

		public function logout()
		{
			$this->model_auth->logout();
			helper_log("Logout", "Logout");
		}
	}
 ?>