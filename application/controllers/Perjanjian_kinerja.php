<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perjanjian_kinerja extends CI_Controller 
    {

    public function index()
        {
            $this->load->view('frontend/_partialsmycss/head.php');
            $this->load->view('frontend/publikasi/perjanjian_kinerja/navbar.php');
            $this->load->view('frontend/publikasi/perjanjian_kinerja/file_upload');
            $this->load->view('frontend/_partialsmycss/alamat.php');
            $this->load->view('frontend/_partialsmycss/footer.php');
            $this->load->view('frontend/_partialsmyjs/js.php'); 
        }
}   
?>
