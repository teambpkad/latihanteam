
<!DOCTYPE html>
<html lang="en">
<head>

<!--Fonts Google-->
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Rajdhani&display=swap" rel="stylesheet">

<!--Icon-->
<link rel="shortcut icon" href="<?php echo base_url('assets/images/iconpemko.ico') ?>">
<title>Halaman Login</title>
<link rel="icon" type="image/png" href="<?php echo base_url('assets/image/logobpkad.png')?>">
<!--Made with love by Mutiullah Samim -->
   
<!--Bootsrap 4 CDN-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
 <!--Fontawesome CDN-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<!-- Animate AOS CSS-->
<link href="<?php echo base_url('assets/css/aos.css') ?>" rel="stylesheet">



<style type="text/css">
html,body{
background-image: url('../assets/images/walikota.jpg');
background-size: cover;
background-repeat: no-repeat;
height: 100%;
font-family: 'Roboto Condensed', sans-serif;
}

.container{
height: 100%;
align-content: center;
}

.card{
height: 280px;
margin-top: auto;
margin-bottom: auto;
width: 400px;
background-color: rgba(0,0,0,0.5)!important;
}

.social_icon span{
font-size: 60px;
margin-left: 10px;
color: #FFC312;
}

.social_icon span:hover{
color: white;
cursor: pointer;
}

.card-header h3{
color: white;
margin-top: 20px;
margin-bottom:-10px  
}

.social_icon{
position: absolute;
right: 20px;
top: -45px;
}

.input-group-prepend span{
width: 50px;
background-color: #FFC312;
color: black;
border:0 !important;
}

input:focus{
outline: 0 0 0 0  !important;
box-shadow: 0 0 0 0 !important;

}

.remember{
color: white;
}

.remember input
{
width: 20px;
height: 20px;
margin-left: 15px;
margin-right: 5px;
}

.login_btn{
color: black;
background-color: orange;
width: 130px;

}

.login_btn:hover{
color: black;
background-color: white;
}

.login_bck{
color: black;
background-color: orange;
width: 220px;

}

.login_bck:hover{
color: black;
background-color: red;
}

.links{
color: white;
}

.links a{
margin-left: 4px;
}
/*<!-- CSS Start -->*/

</style>

</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100" data-aos="zoom-in"
    data-aos-delay="50"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
		<div class="card">
			<div class="card-header">
				<h3 style="font-family: 'Roboto Condensed', sans-serif;"><center>Halaman Login</center></h3>
			</div>
			<?php
	 // Cetak session
				if($this->session->flashdata('sukses')) {
					echo '<p class="warning" style="margin: 10px 20px;">'.$this->session->flashdata('sukses').'</p>';
				}
	// Cetak error
	echo validation_errors('<p class="warning" style="margin: 10px 20px;">','</p>');
	?>
			<?php echo $this->session->flashdata('pesan'); ?>
			<div class="card-body">
				<form method="post" action="<?php echo base_url('auth/login')?>" class="user">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input method="post" type="text" name="username" class="form-control" placeholder="Masukkan Nama User" 
						>
						
					</div> 	
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input method="post" type="password" name="pwd" class="form-control" placeholder="Masukkan Kata Sandi">
					</div>
					
					<div class="form-group">
						<input type="submit" value="Masuk" class="btn float-right login_btn ">
						<!-- <input type="submit" value="Keluar" class="btn float-lefts login_bck "> -->
					</div>
				</form>
				<form method="post" action="<?php echo base_url() ?>" class="user">
					<div class="form-group">
						<input type="submit" value="Kembali Ke Halaman Utama" class="btn float-lefts login_bck ">
					</div>
				</form>
			</div>

		</div>
	</div>
</div>

<script src="<?php echo base_url()?>assets/js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!-- Animate AOS CSS-->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script>
  AOS.init({

    disable: 'mobile'
  });
</script>
</body>
</html>